package markoviantextgenerator.formatters;

/**
 * A formatter that splits a text into a tokens of the same specified length.
 */
public class CharFixedLenFormatter extends TextFormatter {

  int size;

  /**
   * Creates a new formatter.
   * @param size the length of the tokens.
   */
  public CharFixedLenFormatter(int size) {
    this.size = size;
  }

  /**
   * Return the specified text.
   * @param text the input text to be filtered.
   * @return the input text.
   */
  @Override
  String filter(String text) {
    return text;
  }

  /**
   * Splits the specified text into an array of token of the same length.
   * @param text the input text
   * @return an array containing all the tokens that form the input text.
   */
  @Override
  String[] splitter(String text) {
    return text.split("(?<=\\G.{" + size + "})");
  }
}
