package markoviantextgenerator.formatters;

/**
 * An object that specified how to split a text into tokens.
 */
public abstract class TextFormatter {

  /**
   * Filters the forbidden characters from the input string.
   * @param text the input text to be filtered.
   * @return the filtered text.
   */
  abstract String filter(String text);

  /**
   * Splits the input text into an array of strings.
   * @param text the input text
   * @return an array containing all the tokens that form the input text.
   */
  abstract String[] splitter(String text);

  /**
   * Filters and splits the input text in a list of tokens.
   * @param text the input text
   * @return an array containing all the tokens that form the input text.
   */
  public String[] split(String text) {
    return splitter(filter(text));
  }
}
