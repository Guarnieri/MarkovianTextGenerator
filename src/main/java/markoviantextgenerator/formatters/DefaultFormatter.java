package markoviantextgenerator.formatters;

/**
 * A formatter that filters all non alphanumeric character and
 *   splits the resulting text by using the space character.
 */
public class DefaultFormatter extends TextFormatter {

  /**
   * Filters all non alphanumeric character.
   * @param text the input text to be filtered.
   * @return the filtered text.
   */
  @Override
  public String filter(String text) {
    return text.replaceAll("[^a-zA-Z èéàòù0-9]", " ").toLowerCase();
  }

  /**
   * Splits the text by using the space character.
   * @param text the input text
   * @return an array containing all the tokens that form the input text.
   */
  @Override
  public String[] splitter(String text) {
    return text.split("\\s+");
  }
}
