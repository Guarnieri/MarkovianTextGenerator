package markoviantextgenerator.markov;

import markoviantextgenerator.markov.distribution.EmpiricalDistribution;

/**
 * An Object that represent a state of a markovian automa.
 * @param <T> the type of the tokens that form the state.
 */
public class State<T> {

  Object[] objects;

  EmpiricalDistribution<State<T>> nexts;

  /**
   * Creates a new state.
   * @param objects a list of token that identify a state of the markovian automa.
   */
  public State(Object...objects) {
    this.objects = objects;
    this.nexts = new EmpiricalDistribution();
  }

  /**
   * Adds the specified state to the successors list.
   * @param state the state to be added.
   */
  public void add(State state) {
    nexts.add(state);
  }

  /**
   * Pick randomly one of the successor states.
   * @return
   */
  public State<T> getRandomNext() {
    return nexts.getRandomElem();
  }

  /**
   * Checks the equality of two states.
   * @param o the object with which to test the equality.
   * @return true if the two states contain the same tokens.
   */
  @Override
  public boolean equals(Object o) {
    if (!(o instanceof State)) {
      return false;
    }
    Object[] v = ((State) o).objects;
    if (v.length != objects.length) {
      return false;
    }
    for (int i = 0; i < v.length; i++) {
      if (!this.objects[i].equals(v[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Creates a string that represent the state.
   * @return a string that represent the state.
   */
  @Override
  public String toString() {
    StringBuilder res = new StringBuilder();
    for (int i = 0; i < objects.length; i++) {
      res.append(objects[i] + " ");
    }
    return res.substring(0, res.length() - 1);
  }

}
