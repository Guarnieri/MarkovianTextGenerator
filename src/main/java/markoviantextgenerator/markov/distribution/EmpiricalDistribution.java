package markoviantextgenerator.markov.distribution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An Object that represent an empirical distribution of the specified elements.
 * @param <T> the type associated to the elements contained in the distribution.
 */
public class EmpiricalDistribution<T> {

  List<T> states;
  Map<T,Integer> counter;
  Integer tot;

  /**
   * Creates a new empirical distribution.
   */
  public EmpiricalDistribution() {
    this.states = new ArrayList<>();
    this.counter = new HashMap<>();
    this.tot = 0;
  }

  /**
   * Adds the specified element to the distribution.
   * @param elem the element to be added.
   */
  public void add(T elem) {
    if (!states.contains(elem)) {
      states.add(elem);
    }
    counter.put(elem,counter.getOrDefault(elem,0) + 1);
    tot++;
  }

  /**
   * Picks randomly one of the elements contained.
   * @return one of the elements contained.
   */
  public T getRandomElem() {
    if (tot == 0) {
      return null;
    }
    int rand = (int) (Math.random() * tot);
    return states.get(getKnuthShuffle(getBasicArray())[rand]);
  }

  protected int[] getBasicArray() {
    int[] a = new int[tot];
    int k = 0;
    for (int i = 0; i < states.size(); i++) {
      for (int j = 0; j < counter.get(states.get(i)); j++) {
        a[k++] = i;
      }
    }
    return a;
  }

  protected int[] getKnuthShuffle(int[] a) {
    for (int i = 0; i < a.length - 2; i++) {
      int rand = i + (int) (Math.random() * ((a.length - i)));
      int temp = a[i];
      a[i] = a[rand];
      a[rand] = temp;
    }
    return a;
  }
}
