package markoviantextgenerator.markov;

import java.util.ArrayList;
import java.util.List;

/**
 * An Object that represent a markovian automa.
 * @param <T> the type associated to the stats of the automa.
 * @see markoviantextgenerator.markov.State
 */
public class Automa<T> {

  List<State<T>> states;

  /**
   * Creates a new markovian automa.
   */
  public Automa() {
    this.states = new ArrayList<>();
  }

  /**
   * Adds a new state to the automa.
   * @param state the state to be added.
   * @see markoviantextgenerator.markov.State
   */
  public void addState(State<T> state) {
    if (!states.contains(state)) {
      states.add(state);
    }
  }

  /**
   * Adds (if not already contained) the two states in input and
   *   marks the second state as a successor of the first state.
   * @param state1 a state of the automa (or a new state).
   * @param state2 the next state.
   * @see markoviantextgenerator.markov.State
   */
  public void connect(State<T> state1, State<T> state2) {
    addState(state1);
    addState(state2);
    states.get(states.indexOf(state1)).nexts.add(state2);
  }

  /**
   * Picks randomly a one of the automa states.
   * @return a random state of the automa.
   * @see markoviantextgenerator.markov.State
   */
  public State<T> getRandomState() {
    return states.get((int) (Math.random() * states.size()));
  }

}
