package markoviantextgenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import markoviantextgenerator.formatters.DefaultFormatter;
import markoviantextgenerator.formatters.TextFormatter;
import markoviantextgenerator.markov.Automa;
import markoviantextgenerator.markov.State;

/**
 * A text generator based on a markovian model.
 */
public class MarkovianTextGenerator {

  int stateSize;
  Automa<String> automa;

  /**
   * Create a net text generator.
   * @param stateSize the number of tokens those forms a single state of the markovian automa.
   * @param text the input text used to create the markovian automa.
   */
  public MarkovianTextGenerator(int stateSize, String text) {
    this(stateSize, new DefaultFormatter(), text);
  }

  /**
   * Create a net text generator.
   * @param stateSize the number of tokens those forms a single state of the markovian automa.
   * @param file a text file that contains the words used to create the markovian automa.
   * @throws IOException if any error occurring during file operations.
   */
  public MarkovianTextGenerator(int stateSize, File file) throws IOException {
    this(stateSize, readFromFile(file));
  }

  /**
   * Create a net text generator.
   * @param stateSize the number of tokens those forms a single state of the markovian automa.
   * @param formatter an object that splits the input text into tokens.
   * @param file a text file that contains the words used to create the markovian automa.
   * @throws IOException if any error occurring during file operations.
   * @see markoviantextgenerator.formatters.TextFormatter
   */
  public MarkovianTextGenerator(int stateSize,
                                TextFormatter formatter,
                                File file) throws IOException {
    this(stateSize, formatter, readFromFile(file));
  }

  /**
   * Create a net text generator.
   * @param stateSize the number of tokens those forms a single state of the markovian automa.
   * @param formatter an object that splits the input text into tokens.
   * @param text the input text used to create the markovian automa.
   */
  public MarkovianTextGenerator(int stateSize, TextFormatter formatter, String text) {
    this.stateSize = stateSize;
    automa = generateAutoma(formatter.split(text));
  }

  /**
   * Generates a random text based on the markovian automa created using the input text.
   * @param wordsNumber the number of words to generates.
   * @return a String that contains the random text generated.
   */
  public String generateRandomText(int wordsNumber) {
    List<State<String>> res = generateListOfStates(wordsNumber);
    StringBuilder str = new StringBuilder();
    for (State s : res) {
      str.append(s.toString() + " ");
    }
    return str.substring(0, str.length() - 1);
  }

  protected Automa<String> generateAutoma(String[] words) {
    Automa<String> automa = new Automa<>();
    for (int i = 0; i < (words.length - 2 * stateSize); i++) {
      String[] a1 = new String[stateSize];
      String[] a2 = new String[stateSize];
      for (int k = 0; k < stateSize; k++) {
        a1[k] = words[i + k];
        a2[k] = words[i + stateSize + k];
      }
      automa.connect(new State<String>(a1), new State<String>(a2));
    }
    return automa;
  }

  protected List<State<String>> generateListOfStates(int wordsNumber) {
    List<State<String>> res = new ArrayList<>();
    State<String> start = automa.getRandomState();
    for (int k = 0; k < (wordsNumber / stateSize + 1); k++) {
      if (start == null) {
        start = automa.getRandomState();
      }
      res.add(start);
      start = start.getRandomNext();
    }
    return res;
  }

  protected static String readFromFile(File file) throws IOException {
    StringBuilder res = new StringBuilder();
    BufferedReader read = new BufferedReader(new FileReader(file));
    String line;
    while ((line = read.readLine()) != null) {
      res.append(line);
      res.append(System.lineSeparator());
    }
    read.close();
    return res.toString();
  }

}
