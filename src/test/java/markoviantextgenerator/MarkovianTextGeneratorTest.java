package markoviantextgenerator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import markoviantextgenerator.formatters.DefaultFormatter;
import org.testng.annotations.Test;

public class MarkovianTextGeneratorTest {

  @Test
  public void test() {
    MarkovianTextGenerator gen = new MarkovianTextGenerator(3,
        "Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa."
        + " Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo,"
        + " quando un anonimo tipografo prese una cassetta di"
        + " caratteri e li assemblò per preparare un testo campione."
        + " È sopravvissuto non solo a più di cinque secoli, "
        + "ma anche al passaggio alla videoimpaginazione,"
        + " pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60,"
        + " con la diffusione dei fogli di caratteri trasferibili “Letraset”,"
        + " che contenevano passaggi del Lorem Ipsum,"
        + " e più recentemente da software di impaginazione come Aldus PageMaker,"
        + " che includeva versioni del Lorem Ipsum.");
    String str = gen.generateRandomText(20);
    assertThat(str.split(" ").length).isGreaterThanOrEqualTo(20);
  }

  @Test
  public void testReadFromFile() throws IOException {
    File f = File.createTempFile("prova","prova");
    String str =
        "Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa";
    BufferedWriter bf = new BufferedWriter(new FileWriter(f));
    bf.write(str);
    bf.close();
    MarkovianTextGenerator m = new MarkovianTextGenerator(2, new DefaultFormatter(), f);
    assertThat(str.toLowerCase()).contains(m.generateRandomText(2).split(" ")[0]);
  }

  @Test
  public void testReadFromFileOtherCostructor() throws IOException {
    File f = File.createTempFile("prova","prova");
    String str =
        "Lorem Ipsum é un testo segnaposto utilizzato nel settore della tipografia e della stampa";
    BufferedWriter bf = new BufferedWriter(new FileWriter(f));
    bf.write(str);
    bf.close();
    MarkovianTextGenerator m = new MarkovianTextGenerator(2, f);
    assertThat(str.toLowerCase()).contains(m.generateRandomText(2).split(" ")[0]);
  }

  @Test
  public void testReadFromBadFile() {
    File f = new File("prova");
    assertThatThrownBy(() -> MarkovianTextGenerator.readFromFile(f))
        .isInstanceOf(FileNotFoundException.class);
  }


}
