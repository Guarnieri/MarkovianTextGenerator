package markoviantextgenerator;

import static org.assertj.core.api.Assertions.assertThat;

import markoviantextgenerator.formatters.CharFixedLenFormatter;
import markoviantextgenerator.formatters.DefaultFormatter;
import org.testng.annotations.Test;

public class TextFormatterTest {
  @Test
  public void testDefaultFilter() {
    DefaultFormatter formatter = new DefaultFormatter();
    String filtered = formatter.filter("asdfghjkl poiuytrewq,.-123");
    assertThat(filtered).isEqualTo("asdfghjkl poiuytrewq   123");
  }

  @Test
  public void testDefaultSplitter() {
    DefaultFormatter formatter = new DefaultFormatter();
    String[] splitted = formatter.splitter("asdfghjkl poiuytrewq,.-123");
    assertThat(splitted).contains("asdfghjkl", "poiuytrewq,.-123");
  }

  @Test
  public void testCharFixedLenFormatter() {
    CharFixedLenFormatter formatter = new CharFixedLenFormatter(4);
    String[] splitted = formatter.split("ATCGATCGATCGAT");
    assertThat(splitted).contains("ATCG","ATCG","ATCG","AT");
  }
}
