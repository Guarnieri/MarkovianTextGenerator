package markoviantextgenerator.markov;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class StateTest {

  @Test
  public void testStateEquality() {
    State<String> s1 = new State("hello","world");
    State<String> s2 = new State("hello","world");
    assertThat(s1.equals(s2)).isTrue();
  }

  @Test
  public void testStateNotEquality() {
    State<String> s1 = new State("hello","world");
    State<String> s2 = new State("hello","worlds");
    assertThat(s1.equals(s2)).isFalse();
  }

  @Test
  public void testNotEqualityDifferentSizes() {
    State<String> s1 = new State("hello","world");
    State<String> s2 = new State("hello","world","moon");
    assertThat(s1.equals(s2)).isFalse();
  }

  @Test
  public void testStateNotEquality2() {
    State<String> s = new State("hello","world");
    assertThat(s.equals(new Object())).isFalse();
  }

  @Test
  public void testGetRandomNext() {
    State<String> s1 = new State<>("hello");
    State<String> s2 = new State<>("world");
    State<String> s3 = new State<>("moon");
    s1.add(s2);
    s1.add(s3);
    assertThat(s1.getRandomNext()).isIn(s2,s3);
  }

  @Test
  public void testToString() {
    State<String> s = new State<>("hello", "world");
    assertThat(s.toString()).isEqualTo("hello world");
  }
}
