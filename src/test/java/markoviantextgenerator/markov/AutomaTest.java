package markoviantextgenerator.markov;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;

public class AutomaTest {
  @Test
  public void test() {
    Automa<String> a = new Automa();
    State<String> s1 = new State("hello");
    State<String> s2 = new State("world");
    State<String> s3 = new State<String>("moon");
    a.connect(s1, s2);
    a.connect(s1, s3);
    assertThat(a.getRandomState()).isIn(s1,s2,s3);
  }

  @Test
  public void testAddMultipleEqualStates() {
    Automa<String> a = new Automa();
    State<String> s = new State<>("hello","world");
    a.addState(s);
    a.addState(s);
    assertThat(a.states).hasSize(1);
  }

  @Test
  public void testStatesContainedInAutomaAfterConnection() {
    Automa<String> a = new Automa();
    State<String> s1 = new State<>("hello","world");
    State<String> s2 = new State<>("hello","moon");
    a.connect(s1,s2);
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(a.states.contains(s1)).isTrue();
    soft.assertThat(a.states.contains(s2)).isTrue();
    soft.assertAll();
  }

  @Test
  public void testStatesConnection() {
    Automa<String> a = new Automa();
    State<String> s1 = new State<>("hello","world");
    State<String> s2 = new State<>("hello","moon");
    State<String> s3 = new State<>("Lorem","Ipsum");
    a.connect(s1,s2);
    a.connect(s2,s3);
    assertThat(s1.nexts.getRandomElem()).isEqualToComparingFieldByField(s2);
  }
}
