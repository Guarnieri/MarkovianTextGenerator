package markoviantextgenerator.markov.distribution;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;

public class EmpiricalDistributionTest {

  @Test
  public void testAdd() {
    EmpiricalDistribution<Integer> d = new EmpiricalDistribution<>();
    d.add(1);
    d.add(2);
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(d.tot).isEqualTo(2);
    soft.assertThat(d.states.size()).isEqualTo(2);
    soft.assertThat(d.counter.size()).isEqualTo(2);
    soft.assertAll();
  }

  @Test
  public void testAddEqualsValue() {
    EmpiricalDistribution<Integer> d = new EmpiricalDistribution<>();
    d.add(1);
    d.add(2);
    d.add(1);
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(d.tot).isEqualTo(3);
    soft.assertThat(d.states.size()).isEqualTo(2);
    soft.assertThat(d.counter.size()).isEqualTo(2);
    soft.assertAll();
  }

  @Test
  public void testGetBasicArray() {
    EmpiricalDistribution<Integer> d = new EmpiricalDistribution<>();
    d.add(1);
    d.add(2);
    d.add(1);
    assertThat(d.getBasicArray()).containsExactly(0,0,1);
  }

  @Test
  public void testGetKnuthShuffle() {
    EmpiricalDistribution<Integer> d = new EmpiricalDistribution<>();
    d.add(1);
    d.add(2);
    d.add(1);
    d.add(3);
    d.add(3);
    assertThat(d.getKnuthShuffle(d.getBasicArray())).containsExactlyInAnyOrder(0,0,1,2,2);
  }

  @Test
  public void testGetRandomElem() {
    EmpiricalDistribution<String> d = new EmpiricalDistribution<>();
    d.add("hello");
    d.add("world");
    d.add("hello");
    d.add("hello");
    d.add("hello");
    assertThat(d.getRandomElem()).isIn("hello","world");
  }

  @Test
  public void testNullGetRandomElem() {
    EmpiricalDistribution<String> d = new EmpiricalDistribution<>();
    assertThat(d.getRandomElem()).isNull();
  }
}