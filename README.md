# MarkovianTextGenerator
[![pipeline status](https://gitlab.com/Guarnieri/MarkovianTextGenerator/badges/master/pipeline.svg)](https://gitlab.com/Guarnieri/MarkovianTextGenerator/commits/master) [![coverage report](https://gitlab.com/Guarnieri/MarkovianTextGenerator/badges/master/coverage.svg)](https://gitlab.com/Guarnieri/MarkovianTextGenerator/commits/master)

A simple text generator based on a markovian model.

# Code Example
Next code shows how to configure MarkovianTextGenerator to generate a random text based on the input ones:
```java

File f = new File("foo.txt");  
MarkovianTextGenerator m = new MarkovianTextGenerator(4, f);
String str = m.generateRandomText(50);

```
The constructor takes two arguments:
* the number of words those forms a single state of the markovian automa (more this value is high more the generated text is significant, but if it is too high it produce a permutation of the input text)
It is easily possible verifying if a parameter is set using:
* a text file that contains the words used to create the markovian automa

It is possible to specify how to split the source text into tokens by using a TextFormatter.
There are two predefined formatters:
* DefaultFormatter: filters all non alphanumeric chars and splits the text using spaces or tabs (e.g. random generations of simple text)
* CharFixedLenFormatter: splits the text into pieces of the specified length (e.g. random generations of sequences of DNA)

It is also possible to create new formatters by extending the TextFormatter interface.

# Installation
Clone the repository using
```console
git@gitlab.com:Guarnieri/MarkovianTextGenerator.git
```
or
```console
https://gitlab.com/Guarnieri/MarkovianTextGenerator.git
```
if you don't have any ssh key.

To compile the library type the follow commands

```console
cd MarkovianTextGenerator
./gradlew jar
```
You can find the jar file in build/libs.

# Documentation

You can get all the documentation typing the follow command
```console
./gradlew javaDoc
```
You can find the JavaDocs in build/docs/javadoc.
